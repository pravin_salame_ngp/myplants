//
//  TitleView.swift
//  MyPlants
//
//  Created by Jay Gurnani on 12/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import UIKit

class TitleView: UIView {

    static func view() -> TitleView?
    {
        if let barView = Bundle.main.loadNibNamed("TitleView", owner: self, options: nil)?.first as? TitleView
        {
            return barView
        }
        return nil;
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
