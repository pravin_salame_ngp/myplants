//
//  Operation.swift
//  MyPlants
//
//  Created by Jay Gurnani on 16/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import UIKit

protocol SubmitFormDelegate : class{
    func didFormSubmitted(withResponse response : [String : Any]?, error : Error?)
}

class SubmitFormService : Operation {

    var session : URLSession!
    var url : URL!
    var parameters : [String : Any]
    weak var delegate : SubmitFormDelegate?
    
    init(parameters : [String : Any], delegate : SubmitFormDelegate) {
        self.parameters = parameters
        self.delegate = delegate;
    }
    
    override func start() {
        if let url = URL(string: self.getUrl())
        {
            self.url = url;
            self.sendSessionRequest()
        }
    }
    
    
    func getUrl() -> String
    {
        return "http://117.239.200.165/myplant/api/plant.php"
        //return "http://13.126.41.246/myplant/api/plant.php"
    }
    
    func sendSessionRequest()
    {
        session = URLSession.shared
        var urlRequest = URLRequest(url: self.url)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        urlRequest.httpBody = self.getParameterData()
        
        let dataTask = session.dataTask(with: urlRequest) { (responseData, urlResponse, error) in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    self.delegate?.didFormSubmitted(withResponse: nil, error: error!)
                }
            }
            else
            {
                self.parseJsonData(responseData: responseData, fromStringData: false)
            }
            
        }
        dataTask.resume();

    }
    
    
    func parseJsonData(responseData : Data?, fromStringData isStringData : Bool)
    {
        do{
            let response = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
            print(response)
            DispatchQueue.main.async {
                self.delegate?.didFormSubmitted(withResponse: response as? [String : Any], error: nil)
            }
            
        }catch
        {
            print("error in pasing \(error)")
            
            if isStringData
            {
                self.formSubmittionFailed(error)
                return;
            }
            
            guard let response = String(data : responseData!, encoding : .utf8)
            else {
                let error = NSError(domain: "", code: 999, userInfo: [  NSLocalizedDescriptionKey: SERVER_ERROR_MSG])
                self.formSubmittionFailed(error)
                return ;
            }
            
            //Truncate unrequired string
            if let index = response.range(of: "{")
            {
                
                let responseJsonString = response.substring(from: index.lowerBound)
                self.parseJsonData(responseData: responseJsonString.data(using: .utf8), fromStringData: true);
                //            DispatchQueue.main.async {
                //                self.delegate?.didFormSubmitted(withError: error);
                //            }
            }
            else
            {
                let error = NSError(domain: "", code: 999, userInfo: [  NSLocalizedDescriptionKey: SERVER_ERROR_MSG])
                self.formSubmittionFailed(error)
            }
        }

    }
    
    func formSubmittionFailed(_ error : Error?)
    {
        DispatchQueue.main.async {
            self.delegate?.didFormSubmitted(withResponse: nil, error: error);
        }
    }
    
    
    func getParameterData() -> Data?
    {
        do{
            let parameterData = try JSONSerialization.data(withJSONObject: self.parameters, options: .init(rawValue: 0))
            return parameterData;
        }
        catch
        {
            return nil
        }
    }
}
