//
//  InfoView.swift
//  MyPlants
//
//  Created by Jay Gurnani on 17/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import UIKit

class InfoView: UIView {
    
    static func view() -> InfoView
    {
        let view = Bundle.main.loadNibNamed("InfoView", owner: self, options: nil)?.first as! InfoView
        return view;
        
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        
        self.removeFromSuperview();
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
