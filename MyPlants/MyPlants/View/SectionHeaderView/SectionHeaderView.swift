//
//  SectionHeaderView.swift
//  MyPlants
//
//  Created by Jay Gurnani on 15/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import UIKit

class SectionHeaderView: UIView {

    var infoButtonAction : (() -> Void)!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func infoButtonTapped(_ sender: Any) {
        infoButtonAction()
    }

}
