//
//  DistrictModel.swift
//  MyPlants
//
//  Created by Jay Gurnani on 12/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import UIKit

class DistrictModel: NSObject {
    
    private var identifierDictionary : [String : [String : String]]!
    
    static let shared = { () -> DistrictModel in 
        let districtModel = DistrictModel()
        districtModel.fetchData()
        return districtModel
    }

    var dictionary = [String : Array<String>]()
    
    func fetchData()
    {
        
        let path = Bundle.main.path(forResource: "District", ofType: "json")!;
        let contentData = NSData(contentsOfFile: path)! as Data
        
        do {
            let contentArray = try JSONSerialization.jsonObject(with: contentData, options: .init(rawValue: 0)) as! Array<[String : String]>
                    for content in contentArray
                    {
                        
                        var tahasilArray : Array<String>
                        if dictionary[content["name"]!] != nil
                        {
                            tahasilArray = dictionary[content["name"]!]!
                        }
                        else
                        {
                            tahasilArray = Array<String>()
                        }
                        tahasilArray.append(content["tahasil"]!)
                        
                        dictionary[content["name"]!] = tahasilArray;
                        
                    }
            
            print("\(dictionary)")
            self.createIdentifierDictionary(fromArray: contentArray)

        }catch
        {
            print("Unable to read file \(error)")
        }
        
    }
    
    
    private func createIdentifierDictionary(fromArray array : Array<[String:String]>)
    {
        var identifierDictionary = [String : [String : String]]()
        
        for dictionary in array {
            
            identifierDictionary[dictionary["tahasil"]!] = dictionary
            
        }
        
        self.identifierDictionary = identifierDictionary;
    }
    
    
    func getDistId(forTahasil tahasil : String) -> String
    {
        let singleDistRecord = self.identifierDictionary[tahasil]!
        return singleDistRecord["DistrictId"]!
    }
    
    func getTahasilId(forTahasil tahasil : String) -> String
    {
        let singleDistRecord = self.identifierDictionary[tahasil]!
        return singleDistRecord["TalukaId"]!

    }
    
}
