//
//  ViewController.swift
//  MyPlants
//
//  Created by Jay Gurnani on 11/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import UIKit
import Eureka
import CoreLocation


let CAMPAIGN_START_DATE = "01/07/17"
let CAMPAIGN_END_DATE   = "07/07/17"
let TEST_DATE           = "05/07/17"

let SUCCESSFULL_SUBMISSION = "Your plantation details are submitted successfully. Thank you for participating in plantation drive."
let ELEGIBLE_MESSAGE = "You are eligible to submit plantation data only once in a day. Please make sure all details are correct. Do you want to proceed?"
let SERVER_ERROR_MSG = "An error occured while submitting plantation details. Please try after some time."
let DATA_ALREADY_SUBMITTED = "You have already submitted plantation details for the day. Now you can add details tommorow."


class ViewController: FormViewController, SubmitFormDelegate {

    fileprivate var districtModel : DistrictModel!
    private var selectedDistrict : String!
    var locationManager : CLLocationManager!
    var loadingAlert : UIAlertController!

    override func viewDidLoad() {
        super.viewDidLoad()

        districtModel = DistrictModel.shared()
        self.addImageToNavigationBar()
        self.createForm()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.initializeLocationManger()
    }
    
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        let validators = self.form.validate()
        if validators.count == 0
        {
            self.loadingAlert = UIAlertController(title: "", message: ELEGIBLE_MESSAGE, preferredStyle: .alert)
            weak var weakSelf = self
            let alertOkAction = UIAlertAction(title: "ok", style: .cancel, handler: { [weak self] _ in
                weakSelf?.sendSubmitRequest()
            })
            self.loadingAlert.addAction(alertOkAction)
            self.present(loadingAlert, animated: true, completion: nil)
        }
        else
        {
            self.showErrorAlert(withMsg: validators.first!.msg)
        }
    }
    func sendSubmitRequest()
    {
        self.loadingAlert = UIAlertController(title: "Submitting form.", message: "Please wait...", preferredStyle: .alert)
        self.present(loadingAlert, animated: true, completion: nil)
        
        var parameters :  [String : Any] = self.form.values() ?? [String : Any]();
        if let image = parameters["image"] as? UIImage
        {
            let croppedImage = image.resizeImage(width: 300.0, height: 300.0)
            let imageEncodedString = croppedImage.encodeToBase64String()
            parameters["image"] = imageEncodedString;
        }
        
        if let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString
        {
            parameters["personIMEINo"] = deviceIdentifier
        }
        
        parameters["perDay"] = getTodayDate()
        
        
        parameters["perLatitude"] = self.locationManager.location?.coordinate.latitude ?? 0.0
        parameters["perLongitude"] = self.locationManager.location?.coordinate.longitude ?? 0.0
        parameters["perAccuracy"] = self.locationManager.location?.horizontalAccuracy ?? 0.0
        
        parameters["districtId"]  = DistrictModel.shared().getDistId(forTahasil: parameters["talukaId"] as! String)
        parameters["talukaId"]  = DistrictModel.shared().getTahasilId(forTahasil: parameters["talukaId"] as! String)
        
        let operationQueue = OperationQueue()
        let submitFormService = SubmitFormService(parameters: parameters, delegate : self)
        operationQueue.addOperation(submitFormService)
    }
    func exitingApplication()
    {
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        
        Thread.sleep(forTimeInterval: 2.0)
        //exit app when app is in background
        exit(0)
    }
    
    func addImageToNavigationBar()
    {
        if let titleView = TitleView.view()
        {
            self.navigationItem.titleView = titleView
        }
    }
    
    func createForm()
    {

        
        TextRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }
        }
        
        PhoneRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }

        }

        
        IntRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }

        }
        
        ImageRow.defaultCellUpdate = { cell, row in
            
            if !row.isValid {
                cell.textLabel?.textColor = .red
            }

        }
        
        
        
        form
            
            +++ Section(){
                var header = HeaderFooterView<SectionHeaderView>(.nibFile(name: "SectionHeaderView", bundle: nil))
                header.onSetupView = { (view, section) -> () in
                    view.infoButtonAction = {
                        self.showInfoView()
                    }
                    
                    
                }
                
                $0.header = header
    
            }
            
            <<< TextRow() {
                $0.title = "Name (नाव)"
                $0.tag = "perName"
                $0.placeholder = ""
                $0.add(rule: RuleRegExp(regExpr: "^[a-zA-Z ]{4,32}$", allowsEmpty: false, msg: "Enter valid name."))
                $0.validationOptions = .validatesOnChange
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "NameImage")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                })
            
            
            <<< PhoneRow() { (row : PhoneRow) in
                row.title = "Mobile (मोबाइल)"
                row.tag = "perPhone"
                row.placeholder = ""
                row.add(rule: RuleMinLength(minLength: 10, msg : "Enter valid phone number"))
                row.add(rule: RuleRequired(msg : "Enter valid phone number"))
                row.add(rule: RuleMaxLength(maxLength: 10, msg : "Enter valid phone number"))
                row.validationOptions = .validatesOnChange
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "Mobile")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                })
        
            +++ PickerInlineRow<String>() { (row : PickerInlineRow<String>) in
                
                row.title = "District (जिल्हा)"
                row.tag = "districtId"
                row.options = Array(self.districtModel.dictionary.keys).sorted()
                row.add(rule: RuleRequired(msg : "Select District"))

                
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "City")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                    
                    if !row.isValid {
                        cell.textLabel?.textColor = .red
                    }

                })

        
            <<< PickerInlineRow<String>() { (row : PickerInlineRow<String>) in
                
                row.title = "Taluka (तालुका)"
                row.tag = "talukaId"
                row.add(rule: RuleRequired(msg :"Select taluka."))
                

                row.disabled = Eureka.Condition.function(["districtId"], { form -> Bool in
                    
                    if let district = self.form.values()["districtId"] as? String
                    {
                        row.options = self.districtModel.dictionary[district]!
                        form.setValues(["taluka":""])
                        return false
                        //                        row.options = Array(self.districtModel.dictionary[district]).sorted()
                    }
                    else
                    {
                        return true
                    }

                    
                })
                
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "City")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                    
                    if !row.isValid {
                        cell.textLabel?.textColor = .red
                    }

                    

                })
        
        
            <<< TextRow() {
                $0.title = "Village (गाव)"
                $0.tag = "perVillage"
                $0.placeholder = "Village"
                $0.add(rule: RuleRequired(msg : "Village name required."))
                $0.add(rule: RuleRegExp(regExpr: "^[a-zA-Z 0-9,._+-/]{4,32}$", allowsEmpty: false, msg: "Enter valid village."))
                $0.validationOptions = .validatesOnChange
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "City")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                })
        
        
            <<< TextRow() {
                $0.title = "Site Address (पत्ता)"
                $0.tag = "perAddress"
                $0.placeholder = "Address"
                $0.add(rule: RuleRequired(msg : "Site address is mandatory."))
                $0.add(rule: RuleRegExp(regExpr: "^[a-zA-Z 0-9,._+-/]{4,100}$", allowsEmpty: false, msg: "Enter valid village."))
                $0.validationOptions = .validatesOnChange
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "City")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                })
        
        
            +++ IntRow(){
                $0.title = "No. of Participants (सहभागी)"
                $0.tag = "perParti"
                $0.placeholder = "0"
                $0.textFieldPercentage = 0.2
                $0.add(rule: RuleRequired(msg : "Enter number of participants"))
                $0.add(rule: RuleSmallerOrEqualThan(max: 100))
                $0.validationOptions = .validatesOnChange
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "NumberOfImage")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                })

        
            +++ IntRow(){
                $0.title = "Sapling count (वनस्पती संख्या)"
                $0.tag = "perPlants"
                $0.placeholder = "0"
                $0.textFieldPercentage = 0.1
                $0.add(rule: RuleRequired(msg : "Enter sapling count"))
                $0.add(rule: RuleSmallerOrEqualThan(max: 99))
                $0.validationOptions = .validatesOnChange
                }.cellSetup({ (cell, row) in
                    cell.imageView?.image = UIImage(named: "NumberOfImage")
                }).cellUpdate({ (cell, row) in
                    cell.tintColor = UIColor.gray
                })


        +++ ImageRow(){
                $0.title = "Upload Plantation Image"
            $0.tag = "image"
            $0.add(rule: RuleRequired(msg : "Upload Plantation image."))
        }.cellSetup({ (cell, row) in
            cell.imageView?.image = UIImage(named: "UploadImage")
        })
    
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func getTodayDate() -> String
    {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        
        return dateFormatter.string(from: date)
        
    }
    
    
    func initializeLocationManger()
    {
        self.locationManager = CLLocationManager()
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            locationManager.stopUpdatingLocation()
        }
        
        
    }
    
    func getCurrentLocation() -> CLLocation?
    {
        locationManager.requestLocation()
        return locationManager.location
    }
    
    func showInfoView()
    {
        let infoView = InfoView.view()
        infoView.frame = self.view.frame
        
        infoView.center = self.view.center;
        
        self.navigationController?.view.addSubview(infoView)
        
    }
    
    func didFormSubmitted(withResponse response: [String : Any]?, error: Error?) {
        
        self.loadingAlert.dismiss(animated: true) { 
            
            if response != nil && error == nil
            {
                //Success
                if let data = response!["data"] as? [String : Any]
                {
                    let message = data["message"] as! String
                    let isError = data["error"] as! Bool
//                    let status = data["status"] as! Int
                    if isError
                    {
                        self.showErrorAlert(withMsg: message)
                    }
                    else
                    {
                        self.showFormSuccessAlert(message: message)

                    }
                }
                else
                {
                    //let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey : "No response received from server"])
                    let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey : SERVER_ERROR_MSG])
                    self.showFormFailureAlert(error)

                }
            }
            else
            {
                if error != nil
                {
                    self.showFormFailureAlert(error!)
                }
                else
                {
                    //let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey : "No response received from server"])
                    let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey : SERVER_ERROR_MSG])
                    self.showFormFailureAlert(error)
                }
            }
            
        }
        
    }
    
//    func didFormSubmitted(withError error: Error?) {
//            self.loadingAlert.dismiss(animated: true, completion: {
//                
//                if error == nil
//                {
//                    self.showFormSuccessAlert()
//                }
//                else
//                {
//                    self.showFormFailureAlert(error!)
//                }
//            })
//        
//    }
    
    func showErrorAlert(withMsg message : String)
    {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert);
        let alertOkAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showFormSuccessAlert(message : String)
    {
        
        let alertController = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
        let alertOkAction = UIAlertAction(title: "ok", style: .cancel, handler:  { [weak self] _ in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yy"
            let currentDate = NSDate()
            let todayDate = formatter.date(from: formatter.string(from: currentDate as Date))
           
            UserDefaults.standard.setValue(todayDate, forKey: "currentDate")
            UserDefaults.standard.setValue(message, forKey: "successMessage")
            UserDefaults.standard.synchronize()
            
            self?.exitingApplication()
        })
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showCustomSuccessAlert(message : String)
    {
        let alertController = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
        let alertOkAction = UIAlertAction(title: "ok", style: .cancel, handler:  { [weak self] _ in
        })
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showFormFailureAlert(_ error : Error)
    {
        let alertController = UIAlertController(title: "", message: "\(error.localizedDescription)", preferredStyle: .alert)
        let alertOkAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)

    }
}
