//
//  File.swift
//  MyPlants
//
//  Created by Jay Gurnani on 15/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import Foundation
import UIKit

extension UIImage
{
    
    func cropToBounds(width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: self.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cgwidth: CGFloat = CGFloat(width)
        let cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
            posX = ((contextSize.width - cgwidth) / 2)
            posY = ((contextSize.height - cgheight) / 2)
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation);//UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)!
        
        return image
    }
    
    
    func resizeImage(width : CGFloat , height : CGFloat) -> UIImage
    {
        
            var actualHeight = self.size.height;
            var actualWidth = self.size.width;
            let maxHeight : CGFloat = 300.0;
            let maxWidth : CGFloat = 400.0;
            var imgRatio = actualWidth/actualHeight;
            let maxRatio = maxWidth/maxHeight;
            let compressionQuality = 0.5;//50 percent compression
            
            if (actualHeight > maxHeight || actualWidth > maxWidth)
            {
                if(imgRatio < maxRatio)
                {
                    //adjust width according to maxHeight
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = imgRatio * actualWidth;
                    actualHeight = maxHeight;
                }
                else if(imgRatio > maxRatio)
                {
                    //adjust height according to maxWidth
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = imgRatio * actualHeight;
                    actualWidth = maxWidth;
                }
                else
                {
                    actualHeight = maxHeight;
                    actualWidth = maxWidth;
                }
            }
            
            let rect = CGRect(x : 0.0, y : 0.0, width : actualWidth, height : actualHeight);
            UIGraphicsBeginImageContext(rect.size);
            self.draw(in: rect)
            let img = UIGraphicsGetImageFromCurrentImageContext();
            let imageData = UIImageJPEGRepresentation(img!, CGFloat(compressionQuality));
            UIGraphicsEndImageContext();
            
            return UIImage(data: imageData!)!;
            
        

        
    }
    
    
    func encodeToBase64String() -> String
    {
        let imageData = UIImagePNGRepresentation(self)!
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64

    }
}
