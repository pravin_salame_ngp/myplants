//
//  UIColor+ThemeColors.swift
//  MyPlants
//
//  Created by Jay Gurnani on 12/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    
    static let appThemeColor = { () -> UIColor in 
    
        let color = UIColor(red: 0.0, green: 114.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        return color
        
    }
}
