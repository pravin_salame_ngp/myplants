//
//  Date+Between.swift
//  MyPlants
//
//  Created by Demo on 22/06/17.
//  Copyright © 2017 Jay Gurnani. All rights reserved.
//

import Foundation

extension Date {
    func isBetweeen(date date1: Date, andDate date2: Date) -> Bool {
        return date1.compare(self) == self.compare(date2)
    }
}
